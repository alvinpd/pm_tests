/*
 * Take an array of integers and partition it so that all the even
 * integers in the array precede all the odd integers in the array.
 * (bonus: Your solution must take linear time in the size of the
 * array and operate in-place with only a constant amount of extra
 * space.)
 *   sample input:  2,4,7,6,1,3,5,4
 *   sample output: 2,4,6,4,7,1,3,5
 */

#include <stdio.h>
#include <errno.h>

static int part_int(int *i, int len);

int main(int argc, const char * argv[])
{
	int i, len;
	int arr[] = { 2, 4, 7, 6, 1, 2, 5, 3 };

	len = sizeof(arr) / sizeof(arr[0]);

	for (i = 0; i < len; i++)
		printf("%d%s", arr[i], i == (len - 1) ? "" : ",");

	part_int(arr, len);

	printf("\n");
	for (i = 0; i < len; i++)
		printf("%d%s", arr[i], i == (len - 1) ? "" : ",");

	return 0;
}

static int part_int(int *arr, int len)
{
	int *s, *e, tmp;

	if (!arr)
		return -EINVAL;

	s = arr;
	e = arr + len - 1;

	while (s <= e) {
		/* This is an even number; move to the next */
		if ((*s & 0x1) == 0) {
			s++;
			continue;
		}

		/* Next until an even number is found */
		while ((*e & 0x1) == 1)
			e--;

		if (s >= e)
			break;

		/* Swap values with the other side */
		tmp = *s;
		*s = *e;
		*e = tmp;

		s++;
		e--;
	}

	return 0;
}

/* End of file */
